#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096


void debug(const char* fmt, ... );

void* map_pages(void const* addr, size_t length, int additional_flags){
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON | additional_flags, -1, 0);
}

void test_default_malloc(){
    printf("Test 1: simple malloc\n");

    void* mem = heap_init(HEAP_SIZE);
    assert(mem);

    void *block1 = _malloc(5);
    void *block2 = _malloc(1024);

    assert(block1);
    assert(block2);

    heap_term();
}

void test_free_one_block() {
    debug("Test 2: free 1 block\n ");


    void* heap = heap_init(HEAP_SIZE);
    assert(heap);

    void* block1 = _malloc(1000);
    void* block2 = _malloc(2000);
    assert(block1);
    assert(block2);

    _free(block2);
    assert(!block_get_header(block1)->is_free);
    assert(block_get_header(block2)->is_free);

    heap_term();
}

void test_free_two_blocks() {
    debug("Test 3: free 2 blocks\n ");

    void* heap = heap_init(HEAP_SIZE);
    assert(heap);

    void* block1 = _malloc(100);
    void* block2 = _malloc(500);
    void* block3 = _malloc(1000);

    assert(block1);
    assert(block2);
    assert(block3);

    _free(block2);
    _free(block3);

    assert(!block_get_header(block1)->is_free);
    assert(block_get_header(block2)->is_free);
    assert(block_get_header(block3)->is_free);

    heap_term();
}

void test_memory_expand() {
    debug("Test 4: memory expansion\n ");

    void* heap = heap_init(0);
    assert(heap);

    size_t initial_size = ((struct region*) heap)->size;

    void* block = _malloc(5 * HEAP_SIZE);
    assert(block);
    size_t new_size = ((struct region*) heap)->size;

    assert(initial_size < new_size);
    heap_term();
}

void test_different_regions_expand() {
    debug("Test 4: memory expansion in a new region\n ");

    void* heap = heap_init(0);
    assert(heap);

    void* fixed_block = map_pages(HEAP_START, HEAP_SIZE, MAP_FIXED);
    assert(fixed_block != MAP_FAILED);

    void *block1 = _malloc(HEAP_SIZE);
    void *block2 = _malloc(HEAP_SIZE);
    assert(block1);
    assert(block2);
    assert(block1 != fixed_block && block2 != fixed_block);

    _free(block1);
    _free(block2);

    debug_heap(stderr, heap);
    heap_term();
}

int main() {
    test_default_malloc();
    test_free_one_block();
    test_free_two_blocks();
    test_memory_expand();
    test_different_regions_expand();
    return 0;
}





